﻿using System;
using System.Collections.Specialized;
using System.Configuration;

namespace VendsysRegister
{
    class Program
    {
        /*
         * Syntax: VendsysRegister.exe
         * 
         * Description:         What it tries to do:
         *                      1. Retrieve a Customer ID from the Vendsys Registration API
         *                      2. Read the customer credentials (Username, Password, License Key, Market Number) from the application config file
         *                      
         *                      NOTE:   Update the application config file with the required information before executing.
         * */

        static string Token = "DD7362CE-E2B2-452D-8B0A-A24C6F831F2F"; // Security Token issued from Vendsys to VEII

        static void Main(string[] args)
        {
            try
            {
                //var appSettings = ConfigurationManager.GetSection("VendsysRegistrationInfoGroup/VendsysRegistrationInfo") as NameValueCollection;
                var appSettings = ConfigurationManager.AppSettings as NameValueCollection;

                if(appSettings == null)
                {
                    Console.WriteLine("There was an error reading the configuration.");
                    Exit(-1);
                }

                var info = new RegistrationInfo();

                info.LicenseKey = appSettings["LicenseKey"];
                info.MarketNumber = appSettings["MarketNumber"];
                info.Username = appSettings["Username"];
                info.Password = appSettings["Password"];

                var response = registerUser(info);

                if (response == null)
                {
                    Exit(-1);
                }
                else
                {
                    try
                    {
                        Console.WriteLine($"Company Name: {response.CompanyName}");
                        Console.WriteLine($"Market Number: {response.MarketNumber}");
                        Console.WriteLine($"Company ID: {response.CompanyId}");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Exit(-1);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Exit(-1);
            }
        }

        static void Exit(int code)
        {
            Environment.Exit(code);
        }

        static VendsysRegistrationService.RegisterMarketResponse registerUser(RegistrationInfo newInfo)
        {
            try
            {
                var request = new VendsysRegistrationService.RegisterMarketRequest();

                request.LicenseKey = newInfo.LicenseKey;        // Customer's License Key
                request.MarketNumber = newInfo.MarketNumber;    // Customer's Market ID
                request.Username = newInfo.Username;            // Customer's Username
                request.Password = newInfo.Password;            // Customer's Password
                request.SecurityToken = Token;
                request.Version = "1.10";

                VendsysRegistrationService.VendSysLinkClient reqServer = new VendsysRegistrationService.VendSysLinkClient("CustomBinding_IVendSysLink");
                return reqServer.RegisterMarket(request);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
                return null;
            }
        }
    }

    public class RegistrationInfo
    {
        public string LicenseKey { get; set; }
        public string MarketNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
