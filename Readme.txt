﻿Syntax: VendsysRegister.exe

Description:         What it tries to do:
                     1. Retrieve a Customer ID from the Vendsys Registration API
                     2. Read the customer credentials (Username, Password, License Key, Market Number) from the application config file
                        
                     NOTE:   Update the application config file with the required information before executing.